#include "board.h"
#include <iostream>
/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    board[0] = 0;
    board[1] = 0;
    board[2] = 0;
    board[3] = 384;
    board[4] = 576;
    board[5] = 0;
    board[6] = 0;
    board[7] = 0;
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->board[0] = board[0];
    newBoard->board[1] = board[1];
    newBoard->board[2] = board[2];
    newBoard->board[3] = board[3];
    newBoard->board[4] = board[4];
    newBoard->board[5] = board[5];
    newBoard->board[6] = board[6];
    newBoard->board[7] = board[7];
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return (board[y] >> (x*2)) & 3; // see if either bit for that square is nonzero
}

bool Board::get(Side side, int x, int y) {
    // black means first bit 0, second bit 1
    // white means first bit 1, second bit 0
    return (side == BLACK)?(board[y]>>(x*2))&1:(board[y]>>(x*2))&2;
}

void Board::set(Side side, int x, int y) {
    board[y] &= 0xFFFF^(3<<(x*2)); // clear previous state
    board[y] |= (side==BLACK)?1<<(x*2):2<<(x*2); // set new state
}

void Board::clear(int x, int y) {
    board[y] &= 0xFFFF^(3<<(x*2)); // clear previous state
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/**
 * @brief Finds all the possible moves side can make
 */
MoveNode* Board::getPossibleMoves(Side side) {
    MoveNode *toReturn = NULL;
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
            Move *toCheck= new Move(i,j);
            if(checkMove(toCheck, side)) {
                if(toReturn==NULL) toReturn = new MoveNode(toCheck);
                else toReturn->next = new MoveNode(toCheck);
            }
        }
    }
    return toReturn;
}

/**
 * @brief scores the Board mutable using the Heuristic heury
 */
int Board::score(Board *mutatable, Heuristic heury) {
    int sum;// the total score;
    int k=0; // where you are in heury
    for (int i=0; i<8; i++) { // for all the columns
        ushort col = mutatable->board[i]; // get the col
        for (int j=0; j<8; j++) { // for all bitpairs in col
            if(col&1) // add the appropriate weight
                sum += heury.weights[k];
            else if(col&2)
                sum += heury.weights[k+1];
            else
                sum += heury.weights[k+2];
            col >>= 2;
            k += 3;
        }
    }
    return sum;
}

/**
 * @brief Performs minimax, finding the optimal move for side using the Heuristic heury and recursing levels times
 */
MoveNode* Board::minimax(Side side, Heuristic heury, int levels, bool self, int alpha, int beta) {
    Side enemy = (side==BLACK)?WHITE:BLACK; // avoid re-finding the enemy's side
    MoveNode *moves = this->getPossibleMoves(side); // Get all the moves
    if(moves == NULL) return new MoveNode(NULL, NULL); // If there are no moves, return that there are no moves
    if (self) { //i.e. if you're trying to maximize the score
        MoveNode *best = moves, *curr = moves->next; // Go through all the moves
        Board *mutatable = this->copy(); // Need to get a score for best
        mutatable->doMove(best->move, side);
        best->score = score(mutatable, heury);
        if (best->score > alpha)
            alpha = best->score;
        delete mutatable;
        while(curr != NULL) {
            mutatable = this->copy(); // Make the move in on a temp board
            mutatable->doMove(curr->move, side);
            curr->score = score(mutatable, heury);
            if (best->score > alpha)
                alpha = best->score;
            if (beta < alpha) {
                MoveNode *temp = curr->next;
                //delete curr;
                curr = temp;
                delete mutatable;
                continue;
            }
            if(levels < 1) { // if you're done regressing, then simply check the score with best (base case)
                curr->score = score(mutatable, heury);
                if(curr->score > best->score) { // if you've found something better
                    delete best; //bye bye old best
                    best = curr; // curr is now the best!
                    curr = curr->next;
                }
                else { // otherwise, curr is useless
                    MoveNode *temp = curr->next;
                    delete curr;
                    curr = temp;
                }
            }
            else { // otherwise,
                MoveNode *enemyBest = mutatable->minimax(enemy, heury, levels, false, alpha, beta); // get the best move your enemy would have
                if(enemyBest->score < beta)
                    beta = enemyBest->score;
                if(beta < alpha) {
                    MoveNode *temp = curr->next;
                    delete enemyBest;
                    delete curr;
                    curr = temp;
                    delete mutatable;
                    continue;
                }
                mutatable->doMove(enemyBest->move, enemy); // assume this has been made to
                MoveNode *aheadBest = mutatable->minimax(enemy, heury, levels-1, true, alpha, beta); // now recursively see what'd be best from there
                if (aheadBest->score > best->score) { // if its better save it,
                    delete best;
                    best = curr;
                    curr = curr->next;
                }
                else { // if its not, don't save it
                    MoveNode *temp = curr->next;
                    delete curr;
                    curr = temp;
                }
                delete enemyBest; // clean up
                delete aheadBest;
                delete mutatable;
            }
        }
        return best;
    }
    else { // this is looking at how your enemy could best screw you over. This step doesn't recurse.
        MoveNode *best = moves, *curr = moves->next; // Go through all the moves
        Board *mutatable = this->copy(); // Need to get a score for best
        mutatable->doMove(best->move, side);
        best->score = score(mutatable, heury);
        delete mutatable;
        while(curr != NULL) {
            mutatable = this->copy(); // Make the move in on a temp board
            mutatable->doMove(curr->move, side);
            curr->score = score(mutatable, heury);
            if(curr->score < best->score) { // if you've found something better
                delete best; //bye bye old best
                best = curr; // curr is now the best!
                curr = curr->next;
            }
            else { // otherwise, curr is useless
                MoveNode *temp = curr->next;
                delete curr;
                curr = temp;
            }
        }
        return best;
    }
}
/*
MoveNode* Board::minimax(Side side, Heuristic heury, int levels) {
    Side enemy = (side==BLACK)?WHITE:BLACK; // avoid re-finding the enemy's side
    MoveNode *moves = this->getPossibleMoves(side); // Get all the moves
    if(moves == NULL) return NULL; // If there are no moves, return that there are no moves
    MoveNode *best = moves, *curr = moves; // Go through all the moves
    while(curr != NULL) {
        Board *mutatable = this->copy(); // Make the move in on a temp board
        mutatable->doMove(curr->move, side);
        if(levels < 1) { // if you're done regressing, then simply check the score with best
            if(score(mutatable, heury) > best->score) { // if you've found something better
                delete best; //bye bye old best
                best = curr; // curr is now the best!
                curr = curr->next;
            }
            else { // otherwise, curr is useless
                MoveNode *temp = curr->next;
                delete curr;
                curr = temp;
            }
        }
        else { // otherwise,
            MoveNode *tempBest; // to store the best in each if-else branch
            MoveNode *possibles = mutatable->getPossibleMoves(enemy); // Opponent's possible scores
            if (possibles == NULL)  // if they have to pass, make the recursive call
                tempBest = mutatable->minimax(side, heury, levels-1); // this is guarenteed to be your side's best
            else { // otherwise, you need the enemy's best move
                MoveNode *where = possibles, *min = NULL;
                Board *mutilated;
                while(where != NULL) {
                    mutilated = mutatable->copy(); // Get another board to mess with
                    mutilated->doMove(where->move, enemy);
                    where->score = score(mutilated, heury);
                    if (min == NULL) { // make sure you have a min
                        min = where;
                        where = where->next;
                    }
                    else if(where->score < min->score) { // check to see if where is better
                        delete min;
                        min = where;
                        where = where->next;
                    }
                    else { //otherwise, where is useless.
                        MoveNode *temp = where;
                        where = where->next;
                        delete temp;
                    }
                    delete mutilated;
                }
                mutilated = mutatable->copy(); // make the enemy's best move
                mutilated->doMove(min->move, enemy);
                tempBest = mutilated->minimax(side, heury, levels-1); // now find what you'd have from there
                delete min; // finish cleanup
                delete mutilated;
            }
            if (tempBest->score > best->score) { // if tempBest is better, then save it
                delete best;
                best = tempBest;
            }
            else // if not, its useless
                delete tempBest;
            curr = curr->next;
        }
        delete mutatable;
    }
    return best;
}
*/

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    int count = 0;
    for (int i=0; i<8; i++)
        for (int j=0; j<16; j+=2)
           count += (board[i]>>j)&1;
    return count;
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    int count = 0;
    for (int i=0; i<8; i++)
        for (int j=1; j<16; j+=2)
           count += (board[i]>>j)&1;
    return count;
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b')
            set(BLACK, i/8, i%8);
        else if (data[i] == 'w')
            set(WHITE, i/8, i%8);
        else
            clear(i/8, i%8);
    }
}
