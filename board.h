#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
using namespace std;

class Board {
   
private:	
    ushort board[8];
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    void clear(int x, int y);
    bool onBoard(int x, int y);
      
public:
    friend class Player;
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    MoveNode* getPossibleMoves(Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int score(Board *mutatable, Heuristic heury);
    MoveNode* minimax(Side side, Heuristic heury, int levels, bool self, int alpha, int beta);

    void setBoard(char data[]);
};

#endif
