#include "common.h"
#include "player.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

void setHeuristic(Heuristic &heury, char* weightVec) {
    for (int k=0; k<192; k++)
        heury.weights[k] = -13;
    // Top left corner
    int k=0, l, m=10, n=20;
    for (int j=0; j<4; j++) // Lower half
        for (int i=j; i<4; i++) {
            l = 3*(i*8+j);
            heury.weights[l] = weightVec[k++];
            heury.weights[l+1] = weightVec[m++];
            heury.weights[l+2] = weightVec[n++];
        }
    k=0; m=10; n=20;
    for (int i=0; i<3; i++) { // Upper half
        k++; m++; n++;
        for (int j=i+1; j<4; j++) {
            l = 3*(i*8+j);
            heury.weights[l] = weightVec[k++];
            heury.weights[l+1] = weightVec[m++];
            heury.weights[l+2] = weightVec[n++];
        }
     }
     
    
    // Top right corner
    k=0; m=10; n=20;
    for (int j=7; j>3; j--)
        for (int i=7-j; i<4; i++) { // Lower half
            l = 3*(i*8+j);
            heury.weights[l] = weightVec[k++];
            heury.weights[l+1] = weightVec[m++];
            heury.weights[l+2] = weightVec[n++];
        }
    k=0; m=10; n=20;
    for (int i=0; i<4; i++) { // Upper half
        k++; m++; n++;
        for (int j=7-i-1; j>3; j--) {
            l = 3*(i*8+j);
            heury.weights[l] = weightVec[k++];
            heury.weights[l+1] = weightVec[m++];
            heury.weights[l+2] = weightVec[n++];
        }
    }
     
    
    // Bottom left corner
    k=0; m=10; n=20;
    for (int j=0; j<4; j++)
        for (int i=7-j; i>3; i--) { // Upper half
            l = 3*(i*8+j);
            heury.weights[l] = weightVec[k++];
            heury.weights[l+1] = weightVec[m++];
            heury.weights[l+2] = weightVec[n++];
        }
    k=0; m=10; n=20;
    for (int i=7; i>3; i--) { // Lower half
        k++; m++; n++;
        for (int j=7-i+1; j<4; j++) {
            l = 3*(i*8+j);
            heury.weights[l] = weightVec[k++];
            heury.weights[l+1] = weightVec[m++];
            heury.weights[l+2] = weightVec[n++];
        }
    }

    
    // Bottom right corner
    k=0; m=10; n=20;
    for (int j=7; j>3; j--)
        for (int i=j; i>3; i--) { // upper half
            l = 3*(i*8+j);
            heury.weights[l] = weightVec[k++];
            heury.weights[l+1] = weightVec[m++];
            heury.weights[l+2] = weightVec[n++];
        }
    k=0; m=10; n=20;
    for (int i=7; i>4; i--) { // lower half
        k++; m++; n++;
        for (int j=i-1; j>3; j--) {
            l = 3*(i*8+j);
            heury.weights[l] = weightVec[k++];
            heury.weights[l+1] = weightVec[m++];
            heury.weights[l+2] = weightVec[n++];
        }
    }
}

int main(int argc, char** argv) {
    //Heuristic heuryB, heuryW;
    //char blackWeights[30] = {/*Black*/ 100, -10, 60, 60, -10, -2, -2, 1, 1, 1,/*White*/ -100, -2, -60, -60, -2, 0, 0, -1, -1, -1,/*Empty*/ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    //char whiteWeights[30] = {/*Black*/ -100, -2, -60, -60, -2, 0, 0, -1, -1, -1,/*White*/ 100, -10, 60, 60, -10, -2, -2, 1, 1, 1,/*Empty*/ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    //setHeuristic(heuryB, blackWeights);
    //setHeuristic(heuryW, whiteWeights);
    
    char *heuryBVec[30], *heuryWVec[30];
    srand(time(NULL));
    for (int i=0; i<30; i++) {
        heuryBVec[i] = new char[30];
        heuryWVec[i] = new char[30];
        for (int j=0; j<30; j++) {
            heuryBVec[i][j] = rand()%256;
            heuryWVec[i][j] = rand()%256;
        }
    }
    
    int blackWins=0, whiteWins=0;
    
    for (int gen=0; gen<100; gen++) {
        blackWins=0; whiteWins=0;
        for (int i=0; i<30; i++) {
            Heuristic heuryB, heuryW;
            setHeuristic(heuryB, heuryBVec[i]);
            setHeuristic(heuryW, heuryWVec[i]);
            Player *black = new Player(BLACK, heuryB);
            Player *white = new Player(WHITE, heuryW);
            Move *blackMove = NULL, *whiteMove = NULL;
            
            for (int i=0; i<3; i++)
            do {
                if(blackMove != NULL) delete blackMove;
                blackMove = black->doMove(whiteMove, -1);
                if(whiteMove != NULL) delete whiteMove;
                whiteMove = white->doMove(blackMove, -1);
                //white->printBoard(std::cout);
                //std::cout << endl;
            } while(blackMove != NULL && whiteMove != NULL);
        
            int bC = white->board->countBlack(), wC = white->board->countWhite();
            if (bC > wC) {
               char* temp = heuryBVec[i];
               heuryBVec[i] = heuryBVec[blackWins];
               heuryBVec[blackWins++] = temp;
            }
            else if (wC > bC) {
               char* temp = heuryWVec[i];
               heuryWVec[i] = heuryWVec[whiteWins];
               heuryWVec[whiteWins++] = temp;
            }
            delete black;
            delete white;
        }
        
        if (blackWins != 0) for (int i = blackWins; i < 30; i++) {
            int k = rand()%blackWins;
            for (int j=0; j<30; j++) {
                heuryBVec[i][j] = heuryBVec[k][j] + (rand()%20 - 10)/10;
            }
        }
        else for (int i=0; i<30; i++)
            for (int j=0; j<30; j++)
                heuryBVec[i][j] += (rand()%20 - 10)/10;
        
        if (whiteWins != 0) for (int i = whiteWins; i < 30; i++) {
            int k = rand()%whiteWins;
            for (int j=0; j<30; j++) {
                heuryWVec[i][j] = heuryWVec[k][j] + (rand()%20 - 10)/10;
            }
        }
        else for (int i=0; i<30; i++)
            for (int j=0; j<30; j++)
                heuryWVec[i][j] += (rand()%20 - 10)/10;
                
       for (int i=0; i<15; i++) {
           int j=0;
           for (; j<10; j++) {
               char temp = heuryBVec[i][j];
               heuryBVec[i][j] = heuryWVec[i+10][j];
               heuryWVec[i+10][j] = temp;
               temp = heuryBVec[i+10][j];
               heuryBVec[i+10][j] = heuryWVec[i][j];
               heuryWVec[i][j] = temp;
           }
           for (; j<30; j++) {
               char temp = heuryBVec[i][j];
               heuryBVec[i][j] = heuryWVec[i][j];
               heuryWVec[i][j] = temp;
           }
       }
    }
    for (int i=0; i<blackWins; i++) {
        for (int j=0; j<30; j++)
            cout << to_string((int)heuryBVec[i][j]) << " ";
        cout << endl;
    }
    cout << endl;
    
    
    for (int i=0; i<whiteWins; i++) {
        for (int j=0; j<30; j++)
            cout << to_string((int)heuryWVec[i][j]) << " ";
        cout << endl;
    }
    cout << endl;
    
    for (int i=0; i<30; i++) {
        delete[] heuryBVec[i];
        delete[] heuryWVec[i];
    }
    
    return 0;
}

/*    int loc=0;
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
//            cout << "(";
            //for (int k=0; k<3; k++)
                std::cout << to_string((int)heuryB.weights[loc]) << ", ";
                loc+=3;
//            cout << ") ";
        }
        cout << endl;
    }
    cout << endl;
    loc=0;
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
//            cout << "(";
            //for (int k=0; k<3; k++)
                std::cout << to_string((int)black->heury.weights[loc]) << ", ";
                loc+=3;
//            cout << ") ";
        }
        cout << endl;
    }
    cout << endl << endl;
    
    loc=1;
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
//            cout << "(";
            //for (int k=0; k<3; k++)
                std::cout << to_string((int)heuryB.weights[loc]) << ", ";
                loc+=3;
//            cout << ") ";
        }
        cout << endl;
    }
    cout << endl;
    loc=1;
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
//            cout << "(";
            //for (int k=0; k<3; k++)
                std::cout << to_string((int)black->heury.weights[loc]) << ", ";
                loc+=3;
//            cout << ") ";
        }
        cout << endl;
    }
    cout << endl << endl;
    
    loc=2;
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
//            cout << "(";
            //for (int k=0; k<3; k++)
                std::cout << to_string((int)heuryB.weights[loc]) << ", ";
                loc+=3;
//            cout << ") ";
        }
        cout << endl;
    }
    cout << endl;
    loc=2;
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
//            cout << "(";
            //for (int k=0; k<3; k++)
                std::cout << to_string((int)black->heury.weights[loc]) << ", ";
                loc+=3;
//            cout << ") ";
        }
        cout << endl;
    }
    cout << endl << endl;
*/
