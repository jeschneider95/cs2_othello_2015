#ifndef __COMMON_H__
#define __COMMON_H__

#include <cstdlib>

typedef unsigned short ushort;
struct Heuristic {char weights[192];};

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
};

struct MoveNode {
    MoveNode *next = NULL;
    Move *move;
    int score;
    MoveNode(MoveNode *n, Move *m) {next=n; move=m; score=0;}
    MoveNode(Move *m) {next=NULL; move=m; score=0;}
};

#endif
