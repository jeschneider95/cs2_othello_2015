#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Board *board;
    Heuristic heury;
    Side me, them;

    Player(Side side);
    Player(Side side, Heuristic h);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    void printBoard(ostream& strm);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
