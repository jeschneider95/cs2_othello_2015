#include "player.h"
#include <iostream>
#include <climits>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    me = side;
    them = (side==BLACK)?WHITE:BLACK;
    board = new Board();
    
    int epsilon = (side==BLACK)?1:-1;
    int k=0;
    for(int j=0; j<8; j++) {
        for(int i=0; i<8; i++) {
            if ((i == 0 || i == 7) && (j == 0 || j == 7)) { // corners
                heury.weights[k] = 100*epsilon;
                heury.weights[k+1] = -100*epsilon;
                heury.weights[k+2] = 0;
                k+=3;
            }
            else if (((i == 1 || i == 6) && (j == 0 || j == 7)) || ((i == 0 || i == 7) && (j == 1 || j == 6))) { // adjacent to corners
                heury.weights[k] = -10*epsilon;
                heury.weights[k+1] = -2*epsilon;
                heury.weights[k+2] = 0;
                k+=3;
            }
            else if ((i == 1 || i == 6) && (j == 1 || j == 6)) { // diagonal to corners
                heury.weights[k] = -10*epsilon;
                heury.weights[k+1] = -2*epsilon;
                heury.weights[k+2] = 0;
                k+=3;
            }
            else if ((i == 1 || i == 6) || (j == 1 || j == 6)) { // just inside edges
                heury.weights[k] = -2*epsilon;
                heury.weights[k+1] = 0;
                heury.weights[k+2] = 0;
                k+=3;
            }
            else if((i == 0 || i == 7) || (j == 0 || j == 7)) { //edges
                heury.weights[k] = 60*epsilon;
                heury.weights[k+1] = -60*epsilon;
                heury.weights[k+2] = 0;
                k+=3;
            }
            else if((i == 2 || i == 5) || (j == 0 || j == 5)) { // two from edges
                heury.weights[k] = epsilon;
                heury.weights[k+1] = -epsilon;
                heury.weights[k+2] = 0;
                k+=3;
            }
            else {
                heury.weights[k] = 1*epsilon;
                heury.weights[k+1] = -1*epsilon;
                heury.weights[k+2] = 0;
                k+=3;
            }
        }
    }
}


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 * Serves the same purpose as the above, except its given a heuristic. Used for testing.
 */
Player::Player(Side side, Heuristic h) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    me = side;
    them = (side==BLACK)?WHITE:BLACK;
    board = new Board();
    heury = h;
}

/*
 * Destructor for the player.
 */
Player::~Player() {}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    board->doMove(opponentsMove, them); // Process the opponent's moves
    /*MoveNode *possibles = board->getPossibleMoves(me); // Get the possible moves
    
    if (possibles == NULL) return NULL; // Make sure you don't process no moves
    
    // Find the best move
    MoveNode *curr=possibles->next, *best=possibles;
    best->score = board->applyHeuristic(best->move, me, heury);
    while(curr != NULL) {
        curr->score = board->minimax(curr->move, me, heury, 1);
        if (curr->score > best->score)
            best = curr;
        curr = curr->next;
    }
    // Save best move
    Move *toReturn = new Move(best->move->x, best->move->y);
    // Clean up
    curr = possibles;
    while(curr != NULL) {
        best = curr->next;
        delete curr->move;
        delete curr;
        curr = best;
    }*/
    MoveNode *bestie = board->minimax(me, heury, 20, true, INT_MIN, INT_MAX);
    if (bestie->move == NULL) {
        delete bestie;
        return NULL;
    }
    Move *toReturn = new Move(bestie->move->x, bestie->move->y);
    delete bestie->move;
    delete bestie;
    // Make best move
    board->doMove(toReturn, me);
    return toReturn;
}

void Player::printBoard(ostream& strm) {
    //ushort maskW = 32768, maskB = 16384;
    ushort maskW = 2, maskB = 1;
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
            if ((maskW & board->board[j]) != 0)
                strm << "W" << " ";
            else if ((maskB & board->board[j]) != 0)
                strm << "B" << " ";
            else
                strm << "  ";
        }
        maskW <<= 2;
        maskB <<= 2;
        strm << endl;
    }
}
